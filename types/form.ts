import { IMovement, IWorkout } from "../interfaces/workout";

export interface ITextInput {
  name: string,
  type?: string,
  index?: number,
  onChange?: boolean
  label?: string
  defaultValue?: string | number | number[]
  values?: IWorkout | IMovement,
  nestedIndex?: number
}

export interface IMovementForm {
  nestedIndex?: number,
  nestedRounds?: number,
  values: IMovement,
  index: number,
  rounds?: number,
  onChange?: boolean
}