

export const emptyWod = {
  type: '',
  source: '',
  section: '',
  notes: '',
  tags: [],
  movements: [{
    type: 'reps',
    reps: [],
    movement: ''
  }],
  equipment: []
};

export const emptyMovement = {
  weight_units: 'lbs',
  distance_units: 'm'
}