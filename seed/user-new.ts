import { IUser } from '../interfaces/user';

export const emptyUser: IUser = {
  isSignedIn: false,
  uid: undefined,
  isAnon: false,
  isAdmin: false,
  isSub: false,
  allowEmail: false,
  dateAcceptedToc: undefined
};