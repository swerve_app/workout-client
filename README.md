# Workouts project front-end

Built using React-Static - TypeScript Template


Build home page with pre-rendered wods from database
When user bookmarks wod on home page, modify the user.bookmarkedWods on database and through context state.

## Local dev
runs on port 3000
but because everything has to re-render on change, local dev takes a long time to preview changes.

## Deployment
`firebase deploy`

note on node modules: I had to go into `node_modules/react-stagic-plugin-jss/node.api.js` and change `"react-jss/lib/jss"` to `"react-jss/dist/react-jss.cjs.js"`


## React Static
html is pre-rendered from `static.config.js`
other pages built dynamically for each file in `src/pages`
`App.tsx` file starts the firebase auth instance and has a listener so that when firebase auth changes, that user data will be saved to `firebase.currentUser` and `userContext.user` contexts. the user context is therefore just a copy of the firebase current user, available in case a component does not need to load the entire firebase context. 

Because the user context value is kept in App.tsx state, a change to the user context value will re-render components, unlike a change in firebase currentUser

## Path Aliases for Absolute Imports

`react-static-typescript-plugin` supports path aliases [since v3.1](https://github.com/react-static/react-static/pull/963#issuecomment-455596728). It has been set up in this template.

```js
// tsconfig.json
{
  // ...
    "paths": {
      "@components/*": ["src/components/*"]
    },
  // ...
}

```
