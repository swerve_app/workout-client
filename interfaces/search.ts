export interface ISearchBar {
  options: string[]
  label?: string
  onChange?: any
  disabled?: boolean
}