export const enum WodIdFieldStrings {
  bookmarkedWodIds = 'bookmarkedWodIds',
  scheduledWodIds = 'scheduledWodIds'
}
export const enum WodFieldStrings {
  bookmarkedWods = 'bookmarkedWods',
  scheduledWods = 'scheduledWods'
}