export default interface IWodFunctions {
  deleteWod?: any
};

export interface IFilter {
  id?: string[],
  equipment?: string[],
  page?: string[],
  section?: string,
  movementList?: string[]
}