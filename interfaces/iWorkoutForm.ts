import { Gender, IWorkout } from "./workout";

export interface ITypeMovementRounds{
  type?: string,
  nestedIndex?: number,
  index: number,
  movement?: string,
  rounds?: number,
  reps?: number[],
  distance?: Gender,
  cals?: Gender,
  weight?: Gender,
  units?: string,
  time?: string[] | string,
  every?: string,
  nestedRounds?: number
};

export interface IWorkoutCard {
  wod: IWorkout,
  updateUserWods: Function,
  uid?: string,
  isAdmin?: boolean,
  bookmarked?: boolean
}
