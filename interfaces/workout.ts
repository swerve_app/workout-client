export interface Gender {
  m: number[],
  f: number[]
}

export interface IMovement {
  type: string,
  name?: string,
  every?: string,
  movement?: string,
  time?: string[],
  reps?: number[],
  rounds?: number,
  cals?: Gender,
  distance?: Gender,
  weight?: Gender,
  units?: string,
  distance_units?: string,
  weight_units?: string,
  movements?: IMovement[],
  sharedRepSchema?: string
}

export interface IWorkout{
  _id?: string,
  scoring?: string[],
  source?: string,
  section: string,
  name?: string,
  type?: string,
  movement?: string,
  movements?: IMovement[],
  notes?: string,
  tags?: string[],
  time?: string,
  every?: string,
  rounds?: number,
  equipment?: string[],
  displayType?: string,
  sharedRepSchema?: string
}

export interface IWodData {
  wods: IWorkout[], 
  fetching: boolean,
  extinguished: boolean
}