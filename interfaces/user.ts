import { IWorkout } from "../interfaces/workout";

export interface IUser {
  isSignedIn?: boolean,
  isAnon?: boolean,
  isAdmin: boolean,
  isAnonymous?: boolean,
  isSub?: boolean,
  uid?: string,
  bookmarkedWods?: IWorkout[],
  bookmarkedWodIds?: string[],
  scheduledWods?: IWorkout[],
  scheduledWodIds?: string[],
  name?: string,
  email?: string,
  allowEmail?: boolean,
  dateAcceptedToc?: Date
}

export interface IUserContext {
  user: IUser,
  updateUser?: Function
}