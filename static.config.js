import axios from 'axios'
import path from 'path'
import { createGenerateClassName } from '@material-ui/core/styles';
import Document from './src/Document';
import dotenv from 'dotenv';
dotenv.config();

const generateClassName = createGenerateClassName();
// import { Workout } from './types'
// Typescript support in static.config.js is not yet supported, but is coming in a future update!
const key = process.env.API_KEY;

export default {
  entry: path.join(__dirname, 'src', 'index.tsx'),
  Document: Document,
  siteRoot: 'https://fitnssd.com',
  basePath: '/',
  getSiteData: async ({ dev }) => ({
    title: 'Let\'s workout',
    lastBuilt: Date.now(),
    metaDescription: 'Find and schedule HIIT, cross-training, and weightlifting workouts, skills, warmups, and accessory work.' 
  }),
  getRoutes: async () => {
    const { data: wods } = await axios.get(
      `${process.env.HOST}/getWods?section=wod&limit=12`, 
      {headers: {
        'x-auth-token': key
      }}
    );

    return [
      {
        path: '/',
        getData: () => ({
          wods
        }),
        template: 'src/pages/Home'
      }
    ]
  },
  plugins: [
    'react-static-plugin-typescript',
    [
      require.resolve('react-static-plugin-source-filesystem'),
      {
        location: path.resolve('./src/pages'),
      },
    ],
    require.resolve('react-static-plugin-reach-router'),
    require.resolve('react-static-plugin-sitemap'),
    [
      require.resolve('react-static-plugin-jss'),
      {
        providerProps: {
          generateClassName,
        },
      },
    ]
  ],
  devServer: {
    port: 3000,
    host: '127.0.0.1',
  },
}
