import { createContext } from 'react';
import { IUserContext } from '../../interfaces/user';
import { emptyUser } from '../../seed/user-new';


const userObject : IUserContext = {
  user: emptyUser,
  updateUser: () => {}
};

export const UserContext = createContext<IUserContext>(userObject);
