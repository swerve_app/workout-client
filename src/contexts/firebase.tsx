import React from 'react';
import app from 'firebase/app';
import 'firebase/auth';
import { getUserData } from '../functions/user';
import { IUser } from '../../interfaces/user';
import { emptyUser } from '../../seed/user-new';

const config = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.FIREBASE_APP_ID,
  measurementId: process.env.FIREBASE_MEASUREMENT_ID
};

/* eslint no-undef: 0 */
class Firebase {
  auth: any;
  googleProvider: app.auth.GoogleAuthProvider;
  emailProvider: app.auth.EmailAuthProvider;
  deleteUser: Function;

  constructor() {
    app.initializeApp(config);

    /* Helper */
    // this.fieldValue = app.firestore.FieldValue;
    // this.emailAuthProvider = app.auth.EmailAuthProvider;
    /* Firebase APIs */
    this.auth = app.auth();
    this.googleProvider = new app.auth.GoogleAuthProvider();
    this.emailProvider = new app.auth.EmailAuthProvider();
  }

  // *** Merge Auth and DB User API *** //
  onAuthUserListener = (next, fallback) =>
    this.auth.onAuthStateChanged(async authUser => {
      if (authUser) {
        let data : IUser;
        try {
          // TODO: change this to updateUserData and update the lastSignInTime
          data = await getUserData(authUser.uid);
        }
        catch(err){ console.log(err); }
        const isSignedIn = !!authUser && !authUser.isAnonymous;
        const userData: IUser = data || emptyUser;
 
        next({
          ...userData,
          isSignedIn,
          isAnon: authUser.isAnonymous,
          uid: authUser.uid,
        });
      } else {
        fallback();
      }
    });
}

export const FirebaseContext = React.createContext(new Firebase())
export default Firebase;