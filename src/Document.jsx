import React from 'react';

const Document = ({
  Html,
  Head,
  Body,
  children
}) => (
  <Html lang="en-US">
    <Head>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width" />
      <title>FITNSSD</title>
      <meta name="description" content="Find, schedule, and share HIIT, cross-training, and weightlifting workouts, skills, warmups, and accessory work with FITNSSD."/>
      <meta name="robots" content="index, follow"/>
      <meta property="og:url" content="https://fitnssd.com" />
      <meta property="og:description" content="Find, schedule, and share HIIT, cross-training, and weightlifting workouts, skills, warmups, and accessory work with FITNSSD." />
      {/* <link rel="preconnect" href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" crossOrigin="true"/> */}
      <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png"/>
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png"/>
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png"/>
      <link rel="manifest" href="/site.webmanifest"></link>
      <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="8f9adfe8-6087-4f02-b550-1d731ab1bb82" data-blockingmode="auto" type="text/javascript"></script>
      <script id="loadWorker" src="/loadWorker.js" type="text/javascript"/>
  
    </Head>
    <Body>{children}</Body>
  </Html>
);

export default Document;