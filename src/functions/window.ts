
export const signOut = (firebase) => (e) => {
  e.preventDefault();
  firebase.auth.signOut();
  window.location.replace('/')
};

export const showDataInNewTab = data => {
  const newTab = window.open();
  newTab.document.open();
  newTab.document.write('<html><head><title>Workout data</title></head><body><pre>' + JSON.stringify(data, null, 2) + '</pre></body></html>');
  newTab.document.close();
  newTab.focus();
}

