import apiService from '../services/apiService';

const url = `${process.env.HOST}`;

export const findUserWods = async (uid: string) => {
  const getWodsUrl = `${url}/getUserWods/${uid}`;
  const returnedWods = await apiService.getRequest(getWodsUrl);
  return returnedWods.data;
}