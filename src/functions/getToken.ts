export const getToken = (firebase) => (): Promise<string> => {
  return firebase.auth.currentUser?.getIdToken();
};