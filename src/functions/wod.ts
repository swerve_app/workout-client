import apiService from '../services/apiService';
import { IWorkout } from '../../interfaces/workout';
import { IFilter } from '../../interfaces/IWodFunctions';

const url = `${process.env.HOST}`;

export const addWod = async (wod: IWorkout, token: string): Promise<IWorkout> => {
  const savedWod = await apiService.dataRequest(`${url}/addWod`, wod, token);
  return savedWod.data;
}

export const editWod = async (wod: IWorkout, token: string) => {
  if (!wod._id){
    throw new Error('cannot update without an id');
  }
  return apiService.dataRequest(`${url}/editWod`, wod, token);
}

export const deleteWod = async (id: string, token: Promise<string>) => {
  if (!id){
    throw new Error('cannot delete without an id');
  }
  return apiService.deleteRequest(`${url}/deleteWod/${id}`, await token);
}

/**
 * return elements of array joined by comma if value is an array
 */
export const joinValues = (value: string | string[]) => {
  return Array.isArray(value) ? value.join(',') : value;
}

export const getWods = async (filter?: IFilter, exclude?: boolean) => {
  let getWodUrl = `${url}/getWods?limit=24`;

  const query = filter && Object.keys(filter).map(key => {
    const value = filter[key];
    if (value && value.length > 0){
      return key + '=' + joinValues(value)
    }
  }).join('&');

  if (query){
    getWodUrl = getWodUrl.concat(`&${query}`);
  }
  if (exclude){
    getWodUrl = getWodUrl.concat(`&exclude=true`)
  }

  console.log('queryUrl: ', getWodUrl)

  const returnedWods = await apiService.getRequest(getWodUrl);
  return returnedWods.data;
}