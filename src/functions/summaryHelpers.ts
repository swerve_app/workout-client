import { IMovement } from "interfaces/workout";

export const showType = type => {
  return type && !['distance', 'reps', 'rounds', 'time'].includes(type);
}

export const repsNumbers = reps => {
  return reps && Array.isArray(reps) && reps.join('-');
}

export const genderAndRoundsNumbers = genderObject => {
  if (!genderObject.m || genderObject.m.length < 1){
    return repsNumbers(genderObject.f);
  }
  else{
    return `${genderObject.f[0]} / ${genderObject.m[0]}`
  }
}

export const multipleRounds = rounds => rounds && rounds > 1;

export const movementBlurb = (movements: IMovement[], sharedRepSchema: String) => {
  return movements.map(item => {
    return item.movements ? movementBlurb(item.movements, item.sharedRepSchema).join(' \n') :
    `${sharedRepSchema || ''} ${!sharedRepSchema && item.reps?.join('-') || item.cals && genderAndRoundsNumbers(item.cals) || item.distance && genderAndRoundsNumbers(item.distance) || ''} ${item.distance && item.distance_units || ''} ${multipleRounds(item.rounds) ? item.rounds : ''} ${showType(item.type) ? item.type : ''} ${item.movement}`
  })
}

export const workoutBlurb = wod => {
  const showRounds = multipleRounds(wod.rounds);

  return `${wod.name || ''} \n
  ${showRounds ? wod.rounds : ''} ${showRounds ? 'rounds' : ''} ${wod.type || wod.displayType || ''}  \n
  ${wod.time || ''}
  ${wod.sharedRepSchema || ''}
  ${movementBlurb(wod.movements, wod.sharedRepSchema)?.join(' \n') || ''}`
}