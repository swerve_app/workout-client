import React from "react";

function getScrollY(scroller) {
  return scroller ? 
    scroller.pageYOffset !== undefined ? 
    scroller.pageYOffset :
    scroller.scrollTop :
    (document.documentElement || document.body).scrollTop;
}

function defaultTrigger(next, current, other) {
  const { directional = true, threshold = 100 } = other;
  if (directional) {
    return next < current ? false : !!(next > current && next > threshold);
  }
  return next > threshold;
}

const useScrollTrigger = (options) => {

  const { triggerFunc = defaultTrigger, ...other } = options;
  const [ref, setRef] = React.useState();
  const yRef = React.useRef();
  const [trigger, setTrigger] = React.useState(false);

  const handleScroll = React.useCallback(() => {
    const scrollY = getScrollY(ref);
    setTrigger(triggerFunc(scrollY, yRef.current, other));
    yRef.current = scrollY;
  }, [other, ref, triggerFunc]);

  React.useEffect(() => {
    if (document){
      (ref || window).addEventListener("scroll", handleScroll);
    }
    return () => {
      if (document){
        (ref || window).removeEventListener("scroll", handleScroll);
      }
    };
  }, [handleScroll, ref, setRef]);

  return [trigger, setRef];
};

export default useScrollTrigger;
