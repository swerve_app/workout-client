import { AxiosResponse } from 'axios';
import apiService from '../services/apiService';
import { IUser } from 'interfaces/user';

const url = `${process.env.HOST}`;

export const bookmarkWod = (data: {wod_id: string, uid: string, add: boolean}, token: string): Promise<AxiosResponse<IUser>> => {
  return apiService.dataRequest(`${url}/bookmarkWod`, data, token)
};

export const getUserData = async (uid: string): Promise<IUser> => {
  const savedUser = await apiService.getRequest(`${url}/getUser/${uid}`)
  return savedUser.data;
};

export const requestData = async (uid: string, token: Promise<string>): Promise<any> => {
  const response = await apiService.authGetRequest(`${url}/requestData?uid=${uid}`, await token)
  return response.data;
}

export const deleteData = (uid: string, token: string) : Promise<AxiosResponse> => {
  return apiService.authGetRequest(`${url}/deleteData?uid=${uid}`, token);
}

export const updateUser = async (data, token: string) : Promise<IUser> => {
  const response = await apiService.dataRequest(`${url}/updateUser`, data, token);
  return response.data
}