
export class OnSearchBarChange {
  dispatch: any
  search: {
    movementList: string[],
    equipment: string[],
    exclude: boolean
  }

  constructor(dispatch, search){
    this.dispatch = dispatch;
    this.search = search;
  }

  execute(name: string) {
    return (e, newValue: string[]) => {
      e.preventDefault();
      return this.dispatch({
        type: 'UPDATE_SEARCH',
        name,
        value: newValue
      })
    }
  }
}