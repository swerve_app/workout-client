export const equipmentEnum = ['assault bike', 'barbell', 'box', 'dumbbell', 'jump rope', 'kettlebell', 'odd-object', 'peg board', 'pull-up bar', 'rope', 'rower', 'sandbag', 'wall ball']
export const movementsEnum = ['clean', 'jerk', 'snatch', 'deadlift', 'biking', 'rowing', 'squatting', 'running', 'jumping', 'pull-ups', 'hanging', 'core', 'muscle-up', 'climbing', 'swimming']
export const typeEnum = ['', 
  'amrap', 
  'rounds', 
  'emom', 
  'on the minute',  
  'chipper', 
  'cals', 
  'distance', 
  'reps',
  'sets',
  'rest'
];
export const scoringEnum = ['time', 'rounds',  'reps', 'completion', 'cals', 'distance', 'weight'];
export const unitsEnum = ['', 'ft', 'kg', 'lbs', 'm', 'km', 'mi'];
export const sectionEnum = ['','wod', 'warmup', 'skill', 'barbell', 'accessory'];
