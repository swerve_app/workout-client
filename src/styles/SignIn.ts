import styled from "styled-components";

export const SignInSection = styled.div`
  padding: 6%;
  display: flex;
  flex-flow: column;
  align-items: flex-end;
  max-width: 500px;
  margin: 0 auto;
  @media(max-width: 600px){
    align-items: center;
  }
`;

export const SignInText = styled.div`
  flex-grow: 2;
  width: 250px;
  z-index: 1;
  max-width: 500px;
  text-align: center;
  a:hover{
    cursor: pointer;
  }
  padding: 0 10px;
  h1{
    font-size: 4em;
    color: lightgrey;
    text-shadow: 9px -2px 14px black;
  }
  p{
    font-size: 1.5em;
    color: lightgrey;
    text-shadow: -1px 0px 15px #3f51b5;
  }
`;