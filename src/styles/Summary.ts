import styled from "styled-components";

export const SummaryWrapper = styled.div`
  overflow: auto;
  h3{
    margin: 0.1em;
    font-size: 1em;
    font-weight: normal;
  }
  p {
    color: grey;
    margin: 0;
    padding: 0 5px 1px 0;
    overflow: auto;
    word-break: break-word;
  }
  button {
    margin: 10px 0;
  }
`;