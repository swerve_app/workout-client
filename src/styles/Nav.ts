import styled from "styled-components";

const NavWrapper = styled.nav`
  padding: 18px;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  a:hover{
    cursor: pointer;
  }
`;

export default NavWrapper;