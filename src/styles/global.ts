import {createGlobalStyle} from 'styled-components'

const GlobalStyle = createGlobalStyle`
 
  body {
    font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue',
    Helvetica, Arial, 'Lucida Grande', sans-serif;
    font-weight: 300;
    font-size: 16px;
    margin: 0;
    padding: 0;
    font-display: swap;
  }
  a {
    text-decoration: none;
    color: #108db8;
    font-weight: bold;
  }

  img {
    max-width: 100%;
  }

  section {
    padding: 6%;
    padding-top: 60px;
    max-width: 1200px;
    min-height: 100vh;
  }

`

export default GlobalStyle;