export const searchReducer = (state, action) => {
  switch(action.type){
    case 'UPDATE_SEARCH':
      return { ...state , [action.name]: action.value }
    default:
      return state;
  }
}

export const wodReducer = (state, action) => {
  switch (action.type) {
    case 'STACK_WODS':
      return { ...state, wods: [...state.wods, ...action.wods ]}
    case 'REPLACE_WODS': 
      return { ...state, wods: action.wods }
    case 'FETCHING_WODS':
      return { ...state, fetching: action.fetching }
    case 'EXTINGUISHED':
      return { ...state, extinguished: action.value }
    default:
      return state;
  }
};

export const pageReducer = (state, action) => {
  switch (action.type) {
    case 'ADVANCE_PAGE':
      return { ...state, page: state.page + 1 }
    case 'RESET_PAGE':
      return { ...state, page: 0}
    default:
      return state;
  }
}