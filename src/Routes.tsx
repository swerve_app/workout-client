import React from 'react';
import { Routes, addPrefetchExcludes } from 'react-static';
import { Router } from '@reach/router';
import Dynamic from 'containers/Dynamic';
import Wod from './pages/wod';

// Any routes that start with 'dynamic' will be treated as non-static routes
addPrefetchExcludes(['dynamic', 'wods']);

export default () => {
  return (
    <React.Suspense fallback={<em>Loading...</em>}>
      <Router>
        <Dynamic path="dynamic" />
        <Wod path='wods/:wod_id' />
        <Routes path="*" />
      </Router>
    </React.Suspense>
  )
}