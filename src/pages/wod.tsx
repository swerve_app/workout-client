import React, { useState, useEffect } from 'react';
import { emptyWod } from '../../seed/wod-new';
import WorkoutSummary from '../components/workouts/WorkoutSummary';
import { getWods } from '../functions/wod';
import { FacebookShare } from '../components/workouts/WorkoutButtons';
import FloatingAddButton from '../components/FloatingAddButton';
import { IWorkout } from '../../interfaces/workout';
import { IFilter } from '../../interfaces/IWodFunctions';
import EditWorkout from '../containers/EditWorkout';
import withSignedIn from 'containers/withSignedIn';

const Wod = (props) => {
  const { wod_id, user } = props;
  const [wod, updateWod] = useState(emptyWod);

  useEffect(() => {
    const loadWods = async () => {
      const filter : IFilter = { id: [wod_id]};
      const savedWod: IWorkout = await getWods(filter);
      updateWod(savedWod[0]);
    }
    window.scrollTo(0, 0);

    loadWods();
  }, []);

  return (

    <section style={{marginTop: '60px'}}>
      <FacebookShare wod={wod} />
      <WorkoutSummary
        wod={wod}
      />
      { user?.isAdmin && <EditWorkout  wod={wod} /> }
      <FloatingAddButton />
    </section>
  )
}


export default withSignedIn(Wod);
