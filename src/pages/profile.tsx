import React, { useContext, useState } from 'react';
import Button from '@material-ui/core/Button';
import { navigate } from '@reach/router';
import { requestData, deleteData, updateUser } from '../functions/user';
import { showDataInNewTab } from '../functions/window';
import { getToken } from '../functions/getToken';
import { FirebaseContext } from '../contexts/firebase';
import { UserContext } from '../contexts/user';
import CheckboxWithLabel from '../components/form/CheckboxWithLabel';
import { AccountStatus } from '../components/subscriptions/AccountStatus';
import { Plans } from '../components/subscriptions/Plans';

const UserProfile = () => {
  const firebase = useContext(FirebaseContext);
  const { user, updateUser: updateUserContext } = useContext(UserContext);
  const [ isSub, updateIsSub ] = useState(user.isSub)

  if (!user){
    navigate('/')
    return null;
  };

  const [ allowEmail, updateAllowEmail ] = useState(user.allowEmail);
  const [ agree, updateAgree ] = useState(!!user.dateAcceptedToc);


  const sendRequest = async (e) => {
    e.preventDefault();
    const token: Promise<string> = getToken(firebase)();
    const userJson = await requestData(user.uid, token);
    showDataInNewTab(userJson)    
  }

  const deleteRequest = async (e) => {
    e.preventDefault();
    const token: string = await getToken(firebase)();
    if (confirm('are you sure you want to delete all your data, including your account?')){
      await deleteData(user.uid, token);
      window.location.replace('/');
    };
  }

  const toggleAllowEmail = async (e) => {
    e.preventDefault();
    const token: string = await getToken(firebase)();
    console.log('send to server')
    await updateUser({
      uid: user.uid,
      allowEmail: !allowEmail
    }, token);
    updateAllowEmail(!allowEmail);
  }

  const createAccount = async e => {
    e.preventDefault();
    const token = await getToken(firebase)();
    const updatedUser = await updateUser({
      uid: user.uid,
      dateAcceptedToc: new Date(),
      allowEmail,
      isSub
    }, token);

    updateUserContext(updatedUser);
    isSub ? navigate('/subscribe') : navigate('/profile')
  }

  return !!user.dateAcceptedToc ? (
    <section>
      {/* <h1>User Profile</h1> */}
      <h2>Hi {user.name}</h2>

      <div style={{marginTop: '80px'}}>
        <h3>Data & Account</h3>
        <AccountStatus isSub={user.isSub} />

        <div style={{marginTop: '140px'}}>
          <h4>Notification Settings</h4>
          <CheckboxWithLabel 
            primary={true}
            checked={allowEmail || false}
            update={toggleAllowEmail}
            label='Allow FITNSSD to email you with workout recommendations?'
          />
          
        </div>

        <div style={{marginTop: '140px'}}>
          <h4>Data Requests & Deletion</h4>
          <div style={{
            display: 'flex', 
            justifyContent: 'space-between',
            flexWrap: 'wrap'
          }}>
            <Button
              onClick={sendRequest}
              type="button" 
              variant="outlined"
              color='primary'
            >
              Request My Data
            </Button>

            <Button
              onClick={deleteRequest}
              type='button' 
              color='secondary'
            >
              Delete My Data & Account
            </Button>
          </div>
        </div>
      
      </div>
    </section>
  ) : (
    <section>
      <h2>Account Creation</h2>
      <div>
        <CheckboxWithLabel
          checked={agree}
          update={() => updateAgree(!agree)}
          label={<span>I accept the <a href='/tocs' target='_blank'>Terms of Service</a> & <a href='/tocs#privacypolicy' target='_blank'>Privacy Policy</a></span>}
        />
        <h4>Notification Settings</h4>
        <CheckboxWithLabel 
          primary={true}
          checked={allowEmail || false}
          update={() => updateAllowEmail(!allowEmail)}
          label='Allow FITNSSD to email you with workout recommendations?'
        />
      </div>
     
      <Plans isSub={isSub} onSubscribe={updateIsSub(!isSub)}/>

      <div>
        <Button
          onClick={createAccount}
          type='button'
          disabled={!agree}
          variant='outlined'
        >
          Create account
        </Button>
      </div>
    </section>
  )
}
export default UserProfile;
