import React from 'react'

export default () => (
  <section>
    <h1>What's Up</h1>
    <p>Spotted something not right? Drop us a line at <a href="mailto:swerveapp.dev+fitnssd@gmail.com">swerveapp.dev+fitnssd@gmail.com</a>. Thanks!</p>
  </section>
)
