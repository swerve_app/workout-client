import React from 'react';
import { WodFieldStrings, WodIdFieldStrings } from '../../interfaces/enums';
import Workouts from '../containers/Workouts';

const bookmarkedWods = 'bookmarkedWods' as WodFieldStrings.bookmarkedWods;
const bookmarkedWodIds = 'bookmarkedWodIds' as WodIdFieldStrings.bookmarkedWodIds;

const UserWorkouts = () => {

  return (
    <section>
      <h2>Bookmarked wods</h2>
      <Workouts 
        wodField={bookmarkedWods}
        wodIdField= {bookmarkedWodIds}
      />
    </section>
  )
}
export default UserWorkouts;
