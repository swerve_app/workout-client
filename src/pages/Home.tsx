import React from 'react';
import { useRouteData } from 'react-static';
import { IWorkout } from '../../interfaces/workout';
import SplashImage from '../components/SplashImage';
import UnsplashLink from '../components/UnsplashLink';
import SignIn from '../components/SignIn';
import FindAndSeeWorkouts from '../containers/FindAndSeeWorkouts';

const Home = () => {
  console.log('reloading home')
  const { wods: initialWods }: { wods: IWorkout[] } = useRouteData();

  return (
    <>
      <SplashImage src='https://ik.imagekit.io/workout/maryna-yazbeck-Su5W0foVywY-unsplash_dQADtF2KC.jpg'>
        <SignIn />
        <UnsplashLink />
      </SplashImage>

      <FindAndSeeWorkouts 
        initialWods={initialWods}
      />
    </>
  )
}

export default Home;
