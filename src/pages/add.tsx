import React from 'react';
import { IWorkout } from '../../interfaces/workout'
import { emptyWod } from '../../seed/wod-new';
import withSignedIn from '../containers/withSignedIn';
import EditWorkout from '../containers/EditWorkout';
import { navigate } from '@reach/router';

const AddPage = ({user}) => {
  if (!user || !user.isAdmin){
    navigate('/')
    return null;
  }
  const wod : IWorkout = emptyWod;
  return <EditWorkout wod={wod}/>
}

export default withSignedIn(AddPage);
