import axios, { AxiosResponse } from 'axios';

class ApiService{
  headers: any;
  apiHeader: any;
  
  constructor(){
    this.headers = {
      'Content-Type': 'application/json'
    }
    this.apiHeader = {
      ...this.headers,
      'X-Auth-Token': process.env.API_KEY
    }
  }

  authHeader(token){
    return {
      ...this.headers,
      'Authorization': `Bearer ${token}`
    }
  }

  getRequest(url): Promise<AxiosResponse>{
    return axios.request({
      method: 'GET',
      url,
      headers: this.apiHeader
    })
  }

  authGetRequest(url, token): Promise<AxiosResponse> {
    return axios.request({
      method: 'GET',
      url,
      headers: this.authHeader(token)
    })
  }

  dataRequest(url, data, token: string): Promise<AxiosResponse>{
    return axios.request({
      method: 'POST',
      url,
      data,
      headers: this.authHeader(token)
    })
  }

  deleteRequest(url, token) {
    return axios.request({
      method: 'DELETE',
      url,
      headers: this.authHeader(token)
    })
  }
}

export default new ApiService();