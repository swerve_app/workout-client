import React, { useContext, useEffect, useState } from 'react';
import { Root } from 'react-static';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Navigation from './containers/Navigation';
import Footer from './containers/Footer';
import GlobalStyle from './styles/global';
import { getUserData } from './functions/user';
import { signOut } from './functions/window';
import Firebase, { FirebaseContext } from './contexts/firebase';
import { UserContext } from './contexts/user';
import { emptyUser } from '../seed/user-new';
import Routes from './Routes';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      height: '100%'
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
);


function App() {
  console.log('rendering app');
  const firebase = useContext(FirebaseContext);
  const userValues = useContext(UserContext);

  // keep in state so changes will pass down to components
  const [ user, updateUser ] = useState(userValues.user); 
  const savedUser = JSON.parse(localStorage.getItem('authUser'));


  useEffect(() => {
    // WARNING listener function must be defined within useEffect or else
    // component re-renders infinitely
    const listener = firebase.onAuthUserListener(
      authUser => {
        localStorage.setItem('authUser', JSON.stringify(
          {uid: authUser.uid, isAnon: authUser.isAnon}
        ));
        
        updateUser(authUser);
      }, () => {
        localStorage.setItem('authUser', null);
        updateUser(emptyUser);
      }
    );

    async function getUserIfSavedId() {
      console.log('get user if saved id')
      if (savedUser && savedUser.uid && savedUser.isAnon) {
        const savedUserData = await getUserData(savedUser.uid);
        updateUser(savedUserData);
      };
    }

    return () => {
      listener();
      getUserIfSavedId();
    }
  }, []);

  const classes = useStyles();

  return (
    <Root>
      <GlobalStyle />
        <div className={classes.root}>
          <Navigation user={user} signOut={signOut(firebase)}/>
          <main>
            <UserContext.Provider value={{ user, updateUser }} >
              <FirebaseContext.Provider value={firebase} >
                <Routes />
              </FirebaseContext.Provider>
            </UserContext.Provider>
          </main>
          <Footer />
          <script defer id="CookieDeclaration" src="https://consent.cookiebot.com/8f9adfe8-6087-4f02-b550-1d731ab1bb82/cd.js" type="text/javascript" async></script>
        </div>
    </Root>
  )
}

// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.register();

export default App;
