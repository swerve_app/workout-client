import React, { useState } from 'react';
import { useField } from 'formik';
import Grid from '@material-ui/core/Grid';
import CheckboxWithLabel from '../../components/form/CheckboxWithLabel';
import NumberInput from '../../components/form/NumberInput';



const SectionRounds = (props: {rounds: number, type: string, index?: number, nestedIndex?: number}) => {
  const { rounds, type, ...indexProps } = props;
  const [ hasRounds, updateHasRounds ] = useState(rounds !== undefined);
  const [field, meta, helpers] = useField('rounds');

  const onUpdate = () => {
    if(hasRounds){
      helpers.setValue(undefined);
    }
    updateHasRounds(!hasRounds)
  };

  return (
    <Grid container item spacing={3} alignItems='stretch' justify='flex-start'>
      
      { type !== 'rounds' && type !== 'amrap' && type !=='sets' &&
      <Grid item xs={6} sm={4} md={3}>
        <CheckboxWithLabel
          checked={hasRounds}
          update={onUpdate}
          label='Multiple rounds'
        />
      </Grid>
      }
    
      { (hasRounds || type === 'rounds' || type === 'sets') && 
      <Grid item xs={6} sm={4} md={3}>
        <NumberInput 
          name='rounds' 
          label={type === 'sets' ? '' : 'rounds'}
          {...indexProps} 
          onChange={false} 
          defaultValue={props.rounds}
        />
      </Grid> }
    </Grid>
  ) 
}

export default SectionRounds;