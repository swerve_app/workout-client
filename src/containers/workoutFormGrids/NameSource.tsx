import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextInput from '../../components/form/TextInput';
import SelectInput from '../../components/form/SelectInput';
import { typeEnum, sectionEnum } from '../../constants';
import NumberInput from 'components/form/NumberInput';


const NameSource = ({name, source, section, type}) => {
  return (
    <Grid container spacing={3} alignItems='flex-start' justify='flex-start'>
      <Grid item xs={6} sm={4} md={2}>
        <TextInput name='source' onChange={false} defaultValue={source}/>
      </Grid>
      <Grid item xs={6} sm={3} md={2}>
        <SelectInput name='section' array={sectionEnum} defaultValue={section}/>
      </Grid>

      <Grid item xs={6} sm={4} md={2}>
        <TextInput name='name' onChange={false} defaultValue={name}/>
      </Grid>

      <Grid item xs={6} sm={3} md={2}>
        { section === 'barbell' ?   
          <div style={{display: 'flex'}}>
            <NumberInput name='program.week' label='week' />
            <NumberInput name='program.day' label='day' /> 
          </div>
          :
          <SelectInput name='type' array={typeEnum} defaultValue={type}/>
        }
      </Grid>

    </Grid>
  )
};

export default NameSource;