import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { FieldArray } from 'formik';
import MovementForm from '../MovementForm';
import Button from '@material-ui/core/Button';
import { emptyMovement } from '../../../seed/wod-new';
import { IMovement } from 'interfaces/workout';

const useStyles = makeStyles({
  root: {
    marginBottom: '10px'
  },
  addButton: {
    margin: '10px 0'
  }
});

const EditMovements = ({values, remove, nestedIndex}) => {
  if (!values.movements) { return null; }
  const classes = useStyles();

  return values.movements.map(({movements, ...data}: IMovement, index) => {
    return (
      <Grid 
        container item 
        spacing={3}
        xs={12} 
        alignItems='center' 
        justify='flex-start' 
        key={`movement-${index}`}
        className={classes.root}
      >
        <Grid item xs={3} sm={2}>
          <Button
            key={`movement-remove-${index}`}
            type="button"
            variant="outlined"
            color="secondary"
            onClick={() => remove(index)}
          >
            X
          </Button>
        </Grid>
        <Grid item xs={9}>
          <h3>{data.movement || 'Movement'} {nestedIndex === undefined ? null : nestedIndex + 1} {index + 1}</h3>
        </Grid>

      <MovementForm 
        index={ index }
        values={ data }
        rounds={ values.rounds }
        nestedRounds={ data.rounds }
        nestedIndex={ nestedIndex }
        key={`${nestedIndex}-movement-form-${index}`}
      />

      <Movements 
        values={{
          movements,
          rounds: data.rounds || values.rounds
        }} 
        key={`movement-movements-${index}`}
        index={index}
        addMore={nestedIndex === undefined ? true : false}
      />
    </Grid>
  )
  })
};

const Movements = ({values, index, addMore = true}: { values: { rounds?: number, movements: any[]}, addMore?: boolean, index?: string}) => {
  const newMovement = {
    type: 'reps',
    reps: values.movements && values.movements[0] && values.movements[0].reps ? values.movements[0].reps : [],
    ...emptyMovement
  };
  const classes = useStyles();

  return (
    <FieldArray
      name={ index === undefined ? 'movements' : `movements[${index}].movements` }
      validateOnChange={false}
      render={({push, remove}) => (
        <>
        <EditMovements values={values} remove={remove} nestedIndex={index}/>
        { addMore && <Button 
          type="button" 
          variant="outlined"
          className={classes.addButton}
          onClick={() => push(newMovement)}
          >
            { index === undefined ? 'Add a movement or group to the workout' : 'Nest a movement within a group' }
          </Button> 
        }
        </>
      )}
    />
  );
};

export default Movements;