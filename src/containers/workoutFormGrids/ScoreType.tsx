import React from 'react';
import Grid from '@material-ui/core/Grid';
import Scored from '../../components/movements-form/Scored';

const ScoreType = ({scoring}) => {
  return (
    <Grid container item spacing={3} alignItems='center' justify='flex-start'>
      <Grid item xs={12} sm={12} md={6}>
        <Scored scoring={scoring}/>
      </Grid>
    </Grid>
  )
};

export default ScoreType;