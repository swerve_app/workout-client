import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    margin: '10px 0'
  },
});

const WodButtons = ({deleteWod, id, errors, touched, isSubmitting, resetForm}) => {
  const onDelete = (id: string) => e => {
    e.preventDefault();
    deleteWod(id);
    resetForm();
  }
  const classes = useStyles();

  return (
    <Grid container spacing={3} alignItems='flex-start' className={classes.root}>
      <Grid item xs={6}>
        <Button
          type="submit"
          variant='contained'
          color='primary'
          disabled={
            isSubmitting ||
            !!(errors.source && touched.source) ||
            !!(errors.section && touched.section)
          }
        >
          Save WOD!
        </Button>
      </Grid>
      <Grid item xs={6} >
        {deleteWod && <Button
          type="button"
          variant='contained'
          color='secondary'
          onClick={onDelete(id)}
        >
          DELETE WOD
        </Button>}
      </Grid>
    </Grid>
  )
};

export default WodButtons;