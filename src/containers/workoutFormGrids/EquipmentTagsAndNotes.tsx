import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextAreaInput from '../../components/form/TextAreaInput';
import EquipmentArray from '../../components/movements-form/EquipmentArray';
import TagsFieldArray from '../../components/movements-form/TagsFieldArray';


const EquipmentTagsAndNotes = ({equipment, tags}) => {
  return (
    <Grid container spacing={3} alignItems='stretch' justify='flex-start'>
      <Grid item xs={12} sm={12} md={8}>
        <EquipmentArray equipment={equipment} />
      </Grid>
      <Grid item xs={12} >
        <TagsFieldArray tags={tags} />
      </Grid>
      <Grid item xs={12} >
        <TextAreaInput name='notes' />
      </Grid>
    </Grid>
  )
};

export default EquipmentTagsAndNotes;