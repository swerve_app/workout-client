import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextInput from '../../components/form/TextInput';

const Time = ({isOnTheMinute, time, every, label}) => {
  return (
    <Grid container spacing={3} alignItems='flex-start'>
      { isOnTheMinute && 
      <Grid item xs={6} sm={4}>
        <TextInput name='every' index={undefined} defaultValue={every}/> 
      </Grid>
      }
      
      <Grid item xs={6} sm={4}>
        <TextInput 
          name='time' 
          index={undefined} 
          defaultValue={time}
          label={label}
        /> 
      </Grid>
    </Grid>
  )
};

export default Time;