import React, { useContext, useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { UserContext } from '../contexts/user';
import { WodFieldStrings, WodIdFieldStrings } from 'interfaces/enums';
import WorkoutCard from './workoutSummaryGrids/WorkoutCard';
import { getWods } from '../functions/wod';
import { IWorkout } from 'interfaces/workout';

const Workouts = (props: { 
  wodField?: WodFieldStrings, 
  wodIdField?: WodIdFieldStrings, 
  wods?: IWorkout[]}
) => {
  const { user, updateUser }= useContext(UserContext);
  const [ wods, updateWods ]: [ IWorkout[], Function ] = useState(props.wods)
  console.log('wods: ', wods)
  
  useEffect(() => {
    const loadUsersWods = async () => {
      const userWods = user[props.wodField];
      if (userWods && userWods.length > 0){
        console.log('update wods with user saved ones: ', userWods)
        updateWods(userWods)
        // updateUser(user)
      }
      else if (props.wodField && user[props.wodIdField]){
        console.log('load users wods')

        //get scheduled or bookmarked wods
        const retrievedWods: IWorkout[] = await getWods({
          'id': user[props.wodIdField]
        })
        updateUser({[props.wodField]: retrievedWods, ...user});
        updateWods(retrievedWods)
      }
    }
    loadUsersWods();

  }, [user]);

  useEffect(() => {
    if (props.wods?.length > 0){
      console.log('props.wods updated', props.wods)
      updateWods(props.wods)
    }
  }, [props.wods])

  const updateUserWods = ({bookmarked, wod, uid}, isAnon) => {
    let newUserBookmarkedIds = user?.bookmarkedWodIds || [];
    let newUserBookmarkedWods = user?.bookmarkedWods || [];
    
    if (bookmarked){
      newUserBookmarkedIds = user?.bookmarkedWodIds.filter((id) => id !== wod._id);
      newUserBookmarkedWods = user?.bookmarkedWods?.filter(({_id}) => _id !== wod._id);
    }
    else {
      newUserBookmarkedIds.push(wod._id);
      newUserBookmarkedWods.push(wod);
    }

    updateUser({
      uid,
      isAnon,
      bookmarkedWodIds: newUserBookmarkedIds,
      bookmarkedWods: newUserBookmarkedWods,
    })
  };

  if (!wods || wods.length === 0){ return null; }
  return (    
    <Grid container spacing={3}>
      { wods.map(wod => {
        const bookmarked = user?.bookmarkedWodIds?.includes(wod._id);
        return (
          <Grid container item xs={12} sm={6} md={4} key={wod._id}>
            <WorkoutCard 
              wod={wod} 
              uid={ user?.uid }
              updateUserWods={updateUserWods}
              bookmarked={bookmarked}
            />
          </Grid>
        )
      })}
    </Grid>
  
  )
};

export default Workouts;