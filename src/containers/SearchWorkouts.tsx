import React from 'react';
import CheckboxWithLabel from '../components/form/CheckboxWithLabel';
import { SearchBar } from 'components/SearchBar';
import { OnSearchBarChange } from '../functions/OnSearchBarChange';
import { equipmentEnum, movementsEnum, sectionEnum } from '../constants';

const searchStyle = {
  boxShadow: '0 2px 2px -2px rgba(0, 0, 0, 0.14', 
  paddingBottom: '5px',
  marginBottom: '30px'
};

const SearchWorkouts = ({dispatch, search}: {dispatch: Function, search?: any}) => {
  const onChange = new OnSearchBarChange(dispatch, search)
  
  return (
    <div style={searchStyle}>
      <h1>Find a workout</h1>
      
      <SearchBar 
        options={equipmentEnum} 
        label='equipment'
        onChange={onChange.execute('equipment')}
      />
      <CheckboxWithLabel 
        checked={search?.exclude}
        primary
        update={onChange.execute('exclude')}
        label='Exclude workouts with any other equipment?'
      />
      <SearchBar 
        options={movementsEnum}
        label='movement'
        onChange={onChange.execute('movementList')}
      />
      <SearchBar 
        options={sectionEnum}
        label='programming section'
        onChange={onChange.execute('section')}
      />
    </div>
  )
}

export default SearchWorkouts;