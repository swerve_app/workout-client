import React from 'react';
import Grid from '@material-ui/core/Grid';
import TypeMovementRounds from './movementFormGrids/TypeMovementRounds';
import { IMovementForm } from '../../types/form';
import { IMovement } from 'interfaces/workout';
import SetsReps from './movementFormGrids/SetsReps';
import DistanceCals from './movementFormGrids/DistanceCals';
import TimeWeight from './movementFormGrids/TimeWeight';


/**
 * 
 * inputs shared by each movement and sub-movement
 * must pass along values to input or else default values from movements array will not show correctly
 */
const MovementForm = (props: IMovementForm) => {
  const { values, ...indexAndRounds } = props;
  const movementValues: IMovement = indexAndRounds.index && values.movements ? values.movements[indexAndRounds.index] : values;

  return (
    <Grid container item spacing={3}>
      <TypeMovementRounds 
        {...indexAndRounds }
        type={movementValues.type} 
        movement={movementValues.movement}
        rounds={indexAndRounds.nestedRounds}
      />

      <Grid container item spacing={3} alignItems='stretch' justify='flex-start'>
        <SetsReps
          {...indexAndRounds }
          type={movementValues.type}
          reps={movementValues.reps}
        />
        <DistanceCals
          {...indexAndRounds }
          type={movementValues.type}
          distance={movementValues.distance}
          cals={movementValues.cals}
          units={movementValues.distance_units || movementValues.units}
        />
      </Grid>


      <TimeWeight
        {...indexAndRounds }
        type={movementValues.type}
        time={movementValues.time}
        every={movementValues.every}
        weight={movementValues.weight}
        units={movementValues.weight_units || movementValues.units}
      />

    </Grid>
  )
};

export default MovementForm;

