import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { useField } from 'formik';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import TextInput from '../../components/form/TextInput';
import SelectInput from '../../components/form/SelectInput';
import Time from '../../components/movements-form/Time';
import GenderAndRounds from '../../components/movements-form/GenderAndRounds';
import { ITypeMovementRounds } from 'interfaces/iWorkoutForm';

const weightUnits = ['kg', 'lbs', undefined, ''];
const TimeWeight = (props: ITypeMovementRounds) => {
  const { type, every, units, weight } = props;
  const name = props.nestedIndex === undefined ? `movements[${props.index}].weight` : `movements[${props.nestedIndex}].movements[${props.index}].weight`
  const [field, meta, helpers] = useField(name);

  const [ showTime, updateShowTime ] = useState(type === 'time');
  const [ showWeight, updateShowWeight ] = useState(false);

  const onUpdateWeight = () => {
    if (showWeight){
      helpers.setValue({m: [], f: []})
    }
    updateShowWeight(!showWeight)
  };

  return (
    <Grid container item spacing={3} alignItems='flex-start'>

      <Grid container 
        item xs={6} md={4} 
        direction='column' alignItems='flex-start'
        spacing={3}
      >
        <Grid item>
          <FormControlLabel
            control={
              <Switch
                checked={showWeight}
                onChange={onUpdateWeight}
                color="primary"
              />
            }
            label='add weight'
          />
        </Grid>

        <Grid container item spacing={3}>
          { showWeight && 
            <GenderAndRounds 
              defaultValue={weight} 
              name='weight' 
              {...props}
            > 
              <Grid item>
                <SelectInput
                  name='weight_units'
                  defaultValue={units}
                  array={weightUnits}
                  {...props}
                />
              </Grid>
            </GenderAndRounds>
          }
        </Grid>

      </Grid>

      <Grid container item xs={6} md={4} spacing={3}>
        { type !== 'for time' && 
          <Grid item>
            <FormControlLabel
              control={
                <Switch
                  checked={showTime}
                  onChange={() => updateShowTime(!showTime)}
                  color="primary"
                />
              }
              label='add time'
            />
          </Grid> 
        }
       
        <Grid item>
          { showTime && 
              <Time {...props} />
          }
        </Grid>
        <Grid item>
          { type === 'on the minute' && 
            <TextInput name='every' defaultValue={every}/> 
          }
        </Grid>

      </Grid>
    </Grid>
  )
};

export default TimeWeight;