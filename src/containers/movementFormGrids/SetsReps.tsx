import React from 'react';
import Grid from '@material-ui/core/Grid';
import NumberInput from '../../components/form/NumberInput';
import Reps from '../../components/movements-form/Reps';
import { ITypeMovementRounds } from 'interfaces/iWorkoutForm';

const SetsReps = (props : ITypeMovementRounds) => {
  const { type } = props;
  if (type !== 'reps' && type !== 'sets'){ return null; }
  return (
    <Grid container item spacing={3}>
      <Reps {...props}/> 
    </Grid>
  )
};

export default SetsReps; 