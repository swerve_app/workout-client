import React from 'react';
import Grid from '@material-ui/core/Grid';
import SelectInput from '../../components/form/SelectInput';
import GenderAndRounds from '../../components/movements-form/GenderAndRounds';
import { ITypeMovementRounds } from 'interfaces/iWorkoutForm';

const distanceUnits = ['ft', 'm', 'km', 'mi', undefined, ''];

const DistanceCals = (props: ITypeMovementRounds) => {
  const { type, distance , cals} = props;
  return (
    <Grid container spacing={3} direction='column'>
      { (type === 'distance') && 
        <Grid item xs={6} sm={4} lg={3}>
          <GenderAndRounds defaultValue={distance} name='distance' {...props}> 
            <SelectInput
              {...props}
              name='distance_units'
              label='units'
              array={distanceUnits}
              defaultValue={props.units}
            />
          </GenderAndRounds>
        </Grid>
        
      }
  
      { (type === 'cals') && 
        <Grid item xs={6} sm={4} lg={3}>
          <GenderAndRounds  
            defaultValue={cals} 
            name='cals' 
            {...props}
          />
        </Grid>
      }
    </Grid>
  )
};

export default DistanceCals;