import React from 'react';
import Grid from '@material-ui/core/Grid';
import SelectInput from '../../components/form/SelectInput';
import TextInput from '../../components/form/TextInput';
import { ITypeMovementRounds } from 'interfaces/iWorkoutForm';
import SectionRounds from 'containers/workoutFormGrids/SectionRounds';
import { typeEnum } from '../../constants';

const TypeMovementRounds = (props: ITypeMovementRounds) => {
  const { type, nestedIndex, index, movement, rounds } = props;
  return (
    <Grid container item spacing={3} alignItems='flex-start' justify='flex-start'>
      <Grid item xs={6} sm={4} lg={3}>
        <TextInput name='movement' defaultValue={movement} nestedIndex={nestedIndex} index={index}/>
      </Grid>
      <Grid item xs={6} sm={2} lg={2}>
        <SelectInput 
          name='type' 
          defaultValue={type} 
          array={typeEnum}
          nestedIndex={nestedIndex}
          index={index} 
        />
      </Grid>

      { type !== 'reps' && <SectionRounds 
        rounds={rounds} 
        type={type} 
        index={index} 
        nestedIndex={nestedIndex} 
      /> }
    </Grid>
  )
};

export default TypeMovementRounds;