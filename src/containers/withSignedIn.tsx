import React from 'react';
import { UserContext } from '../contexts/user';

const withSignedIn = (Component: React.ComponentType<any>) => {
  return props => (
    <UserContext.Consumer>
      { value => (
        value.user?.isSignedIn ? <Component user={value.user}  {...props}/> : null
      )}
    </UserContext.Consumer>
  );
};

export default withSignedIn;