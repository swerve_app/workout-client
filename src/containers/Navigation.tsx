import React from 'react';
import { Link } from '@reach/router';
import { Slide } from "@material-ui/core";
import AppBar from '@material-ui/core/AppBar';
import FaceIcon from '@material-ui/icons/Face';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import useScrollTrigger from '../functions/useScrollTrigger';


const Navigation = ({user, signOut}) => {
  const [trigger, setRef] = useScrollTrigger({ threshold: 200 });

  return (
    <Slide appear={false} in={!trigger}>
      <AppBar>
        <Toolbar>
          {/* <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton> */}
          <Button>
            <Link to="/" style={{'color': '#fff'}}>
              FITNSSD
            </Link>
          </Button>
          <Button style={{flexGrow: 1, justifyContent: 'flex-start'}}>
            <Link to='/userWorkouts' style={{'color': '#fff'}}>
              My workouts
            </Link>
          </Button>
          { user?.isSignedIn && 
            <IconButton
              aria-label="account of current user"
              aria-controls="menu-appbar"
              color="inherit"
              style={{flexGrow: 1}}
            >
              <Link to='/profile' style={{'color': '#fff'}}>
                <FaceIcon />
              </Link>
            </IconButton>
          }
         {user?.isSignedIn && 
          <Button 
            onClick={signOut}
          >Logout</Button>}
        </Toolbar>
      </AppBar> 
    </Slide>
  )
};

export default Navigation;