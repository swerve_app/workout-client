import React, { useReducer, useEffect } from 'react';
import { Button } from '@material-ui/core';
import SearchWorkouts from './SearchWorkouts';
import Workouts from './Workouts';
import FloatingAddButton from '../components/FloatingAddButton';
import ProgressCircle from '../components/ProgressCircle';
import { getWods } from '../functions/wod';
import { wodReducer, searchReducer, pageReducer } from '../reducers';
import { IWorkout, IWodData } from 'interfaces/workout';

const FindAndSeeWorkouts = ({initialWods}) => {
  const [ wodData, wodDispatch ]: [IWodData, any] = useReducer(wodReducer,{ 
    wods:initialWods, 
    fetching: false,
    extinguished: false
  });

  const [ search, searchDispatch ] = useReducer(searchReducer, undefined);
  const [ pager, pagerDispatch ] = useReducer(pageReducer, { page: 0 });

  // get more wods when page changes
  useEffect(() => {
    const loadWods = async () => {
      wodDispatch({ type: 'FETCHING_WODS', fetching: true });
      try{
        console.log('call find wods ', pager.page)
        const { exclude, ...searchTerms } = search;
        const wods: IWorkout[] = await getWods({
          page: [pager.page + 1],
          ...searchTerms
        }, exclude);
        console.log('stack wods: ', wods);
        wodDispatch({ type: 'STACK_WODS', wods});
        wodDispatch({ type: 'FETCHING_WODS', fetching: false});
        wodDispatch({ type: 'EXTINGUISHED', value: wods.length < 12})
      }
      catch(err){
        wodDispatch({ type: 'FETCHING_WODS', fetching: false })
      }
    }
    if (pager.page > 0){
      loadWods();
    }
  }, [ pager.page ]);

  // get more wods when search bars change
  useEffect(() => {
    const filterWods = async () => {
      wodDispatch({ type: 'FETCHING_WODS', fetching: true });
      pagerDispatch({ type: 'RESET_PAGE'});
      const { exclude, ...searchTerms } = search;
      try{
        const wods: IWorkout[] = await getWods({
          ...searchTerms
        }, exclude);
        console.log('replace wods: ', wods);
        wodDispatch({ type: 'REPLACE_WODS', wods});
        wodDispatch({ type: 'FETCHING_WODS', fetching: false});
        wodDispatch({ type: 'EXTINGUISHED', value: wods.length < 12})
      }
      catch(err){
        wodDispatch({ type: 'FETCHING_WODS', fetching: false })
      }
    }
    if (search){
      filterWods();
    }
  }, [ search ]);

  return (
    <section>
      <SearchWorkouts 
        search={ search }
        dispatch={ searchDispatch }
      />
      <ProgressCircle loading={wodData.fetching} />

      <Workouts wods={wodData.wods} /> 
      <FloatingAddButton />
      {!wodData.extinguished && <Button 
        variant='outlined' 
        onClick={() => pagerDispatch({ type: 'ADVANCE_PAGE'})}
      >
        load more workouts
      </Button>}
      
      {wodData.fetching && (
        <div className="text-center bg-secondary m-auto p-3">
          <p className="m-0 text-white">Getting more workouts</p>
        </div>
      )}

      {wodData.wods.length === 0 && <p>Sorry, nothing found</p>}
    </section>
  )
};

export default FindAndSeeWorkouts;