import { withFormik } from 'formik';
import * as Yup from "yup";
import { navigate } from '@reach/router';
import { IWorkout } from '../../interfaces/workout';
import WorkoutForm from './WorkoutForm';
import IWodFunctions from '../../interfaces/IWodFunctions';

type WorkoutFormProps = {
  wod: IWorkout 
}

export default (onSubmit, getToken: Function) => withFormik<IWodFunctions & WorkoutFormProps, IWorkout>({
  mapPropsToValues: props => ({...props.wod}),

  validationSchema: Yup.object().shape({
    type: Yup.string().required(''),
    movement: Yup.string(),
    notes: Yup.string()
  }),

  async handleSubmit(
      wod: IWorkout,
      { setErrors, resetForm }
  ) {
    try{
      console.log('saving wod: ', wod);
      const token : string = await getToken();
      const savedWod = await onSubmit(wod, token);
      window.alert(`saved wod ${savedWod._id}`);
      navigate(`/wods/${savedWod._id}`)
    }
    catch(err) {
      setErrors(err);
      alert(err);
      console.log(err);
    }
  }
})(WorkoutForm);