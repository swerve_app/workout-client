import React from 'react';
import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';
import { Link } from '@reach/router';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    footer: {
      bottom: '0',
      backgroundColor: theme.palette.primary.main,
      padding: '16px',
      fontSize: '0.8em',
      display: 'flex',
      justifyContent: 'space-between'
    },
  }),
);

const Footer = () => {
  const classes = useStyles();

  return (
    <footer className={classes.footer}>
      <Link to='/contact' style={{'color': '#fff', fontWeight: 'lighter'}}>
        Contact Us
      </Link>
      <Link to='/tocs' style={{'color': '#fff', fontWeight: 'lighter'}}>
        Terms & Conditions
      </Link>
    </footer>
  )
};

export default Footer;