import React, { useContext } from 'react'
import Form from './Form';
import { addWod, editWod, deleteWod } from '../functions/wod';
import { getToken } from '../functions/getToken';
import { FirebaseContext } from '../contexts/firebase';
import { IWorkout } from '../../interfaces/workout'

const EditWorkout = ({ wod}: { wod: IWorkout }) => {
  const firebase = useContext(FirebaseContext);
  const onSubmit = !!wod?._id ? editWod : addWod;
  const EditForm = Form(onSubmit, getToken(firebase));
  const title = !!wod?._id ? 'Edit workout': 'Add a workout';

  return (
    <section>
      <h1>{title}</h1>
      <EditForm wod={wod} deleteWod={deleteWod} />
    </section>
  )
}

export default EditWorkout;