import React, { useContext, useState } from 'react';
import WorkoutSummary from '../../components/workouts/WorkoutSummary';
import BookmarkButtons from '../../components/workouts/WorkoutButtons';
import SimpleModal from '../../components/Modal';
import { bookmarkWod } from '../../functions/user';
import { FirebaseContext } from '../../contexts/firebase';
import { WorkoutCardWrapper } from '../../styles/WorkoutCard';
import { IWorkoutCard } from 'interfaces/iWorkoutForm';


const WorkoutCard = (props: IWorkoutCard) => {
  const { 
    wod, 
    updateUserWods,
    uid
  } = props;

  const firebase = useContext(FirebaseContext);
  const [open, setOpen] = useState(false);
  const [ bookmarked, updateBookmarked] = useState(props.bookmarked);
  const id = wod._id;
  
  const onBookmark = async (e) => {
    e.preventDefault();
    try{

      let userId = uid;
      let isAnon = false;
      if (!uid) {
        const anonUser = await firebase.auth.signInAnonymously();
        userId = anonUser.user.uid;
        isAnon = true;
      }
      updateUserWods({bookmarked, wod, uid: userId}, isAnon);
      updateBookmarked(!bookmarked);
      const token: string = await firebase.auth.currentUser.getIdToken();
      await bookmarkWod({wod_id: wod._id, uid: userId, add: !bookmarked}, token);
    }
    catch(err){
      console.log(err);
    }
  }

  const toggleModal = (e) => {
    setOpen(!open)
  }

  return (
    <WorkoutCardWrapper>
      <BookmarkButtons 
        wod={wod}
        bookmarked={bookmarked}
        onBookmark={onBookmark}
      />

      <div onClick={toggleModal}>
        <WorkoutSummary 
          wod={wod} 
          key={id} 
        />
      </div>
      <SimpleModal open={open} close={() => setOpen(false)}>
        <WorkoutSummary 
          wod={wod} 
          key={id} 
          showNotes={true}
        />
      </SimpleModal>
    </WorkoutCardWrapper>
  )
};

export default WorkoutCard;