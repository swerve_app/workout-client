import React from 'react';
import { FormikProps } from "formik";
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import NameSourceSection from './workoutFormGrids/NameSource';
import Rounds from './workoutFormGrids/SectionRounds';
import ScoreType from './workoutFormGrids/ScoreType';
import Time from './workoutFormGrids/Time';
import Movements from './workoutFormGrids/Movements';
import EquipmentTagsAndNotes from './workoutFormGrids/EquipmentTagsAndNotes';
import WodButtons from './workoutFormGrids/WodButtons';
import { IWorkout } from '../../interfaces/workout';
import IWodFunctions from 'interfaces/IWodFunctions';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    }
  }),
);

const WorkoutForm = (props: IWodFunctions & FormikProps<IWorkout>) => {
  const {
    errors,
    touched,
    handleSubmit,
    isSubmitting,
    resetForm,
    values: { movements, ...values }
} = props;


const classes = useStyles();

return (
  <form key={values._id} onSubmit={handleSubmit} className={classes.root}>
    <NameSourceSection
      name={values['name']} 
      source={values['source']} 
      section={values.section}
      type={values.type}
    />

    <Rounds 
      rounds={values.rounds} 
      type={values.type}
    />

    <Time 
      time={values.time} 
      isOnTheMinute={values.type === 'on the minute'} 
      every={values.every}
      label={values.type === 'on the minute' ? 'for' : values.scoring && values.scoring.includes('time') ? 'time cap' : 'time'}
    />

    <ScoreType
      scoring={values.scoring}
    />

    <Movements
      values = {{
        rounds: values.rounds,
        movements
      }}
    />
 
    <EquipmentTagsAndNotes 
      equipment={values.equipment} 
      tags={values.tags} 
    />

    <WodButtons 
      deleteWod={props.deleteWod} 
      id={values._id} 
      isSubmitting={isSubmitting} 
      errors={errors} 
      touched={touched}
      resetForm={resetForm}
    /> 
  
  </form>
);
}
export default WorkoutForm
