import React from 'react';
import { Field } from "formik";
import TextField from '@material-ui/core/TextField'

const TextAreaInput = ({name}) => {
  return (
    <Field
      validateOnBlur
      name={name}
    >
      {({ form }) => (
        <TextField
          name={name}
          multiline
          error={
            Boolean(form.errors[name] && form.touched[name])
          }
          onChange={form.handleChange}
          onBlur={form.handleBlur}
          variant='outlined'
          label={name}
          defaultValue={form.values[name]}
        />

      )}
    </Field>

  )
};

export default TextAreaInput;