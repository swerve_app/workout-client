import React from 'react';
import TextInput from './TextInput';
import { ITextInput } from 'types/form';

const NumberInput = (props: ITextInput) => {
  return (
    <TextInput 
      type='number' 
      defaultValue={Number(props.defaultValue).toFixed(2)}
      {...props}
    />
  )
};

export default NumberInput;