import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const CheckboxWithLabel = ({checked, update, label, primary=false}) => {
  return (
    <FormControlLabel
      control={
        <Checkbox 
          checked={checked} 
          color={primary ? 'primary' : 'secondary'}
          onChange={update} 
        />}
      label={label}
    />
 
  )
};



export default CheckboxWithLabel;