import React from 'react';
import { Field } from 'formik';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const SelectInput = (props) => {
  const name = props.nestedIndex === undefined ? props.index === undefined ? props.name : `movements[${props.index}].${props.name}` : `movements[${props.nestedIndex}].movements[${props.index}].${props.name}`;

  return (
    <Field
      name={name}
    >
      {({form}) => (
          <FormControl variant="outlined" style={{minWidth: 120}}>
      
          <InputLabel id={`select-${props.name}-${props.index}`}>{props.name}</InputLabel>
          <Select
            label={props.name}
            name={name}
            value={props.defaultValue}
            onChange={form.handleChange}
            variant='outlined'
            autoWidth
          >
            { props.array.map((text, index) => {
                return (
                  <MenuItem 
                  key={`menuitem-${text}-${index}`} 
                  value={text}>
                    {text}
                  </MenuItem>
                )
              })
            }
          </Select>
        </FormControl>        
      )}
  </Field>
  )
};

export default SelectInput;