import React from 'react';
import { Field } from "formik";
import TextField from '@material-ui/core/TextField';
import { ITextInput } from 'types/form';

const TextInput = (props : ITextInput) => {
  const onBlur = form => e => {
    e.preventDefault();
    form.handleBlur(e);
    form.handleChange(e);
  };
  const name = props.index === undefined ? props.name : props.nestedIndex === undefined ? `movements.${props.index}.${props.name}` : `movements[${props.nestedIndex}].movements[${props.index}].${props.name}`;

  return (
      <Field
        validateOnBlur
        // validateOnChange
        name={props.name}
      >
        {({ form }) => {
          return (
            <TextField
              name={name}
              type={props.type || 'text'}
              error={
                Boolean(form.errors[name] && form.touched[name])
              }
              onChange={props.onChange ? form.handleChange : null}
              onBlur={props.onChange ? form.onBlur : onBlur(form)}
              variant='outlined'
              label={props.label || props.name}
              defaultValue={props.defaultValue}
            />
          )
        }
      }
      </Field>
   
  )
};

export default TextInput;