import React, { useEffect, useState } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingTop: '60px',
      paddingBottom: '100px',
      backgroundPosition: 'right',
      backgroundSize: 'cover',
      height: '60vh',
      [theme.breakpoints.down('sm')]: {
        height: '100vh',
        backgroundPosition: 'center'
      }
    }
  }),
);

const SplashImage = (props) => {
  const [ src, updateSrc ] = useState(props.src);

  // useEffect(() => {
  //   updateSrc()
  // }, [])

  const style = {
    'backgroundImage': `url("${src}")`
  };
  const classes = useStyles();

  return (
    <div className={classes.root} style={style}>
      {props.children}
    </div>
  )
}

export default SplashImage;