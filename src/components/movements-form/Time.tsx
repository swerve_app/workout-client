import React, { useState } from 'react';
import TextInput from '../form/TextInput';
import { ITypeMovementRounds } from 'interfaces/iWorkoutForm';
import CheckboxWithLabel from 'components/form/CheckboxWithLabel';


const Time = ({rounds = 1, time, ...props}: ITypeMovementRounds) => {
  const { index, nestedIndex } = props;
  const initialState = !time || (time && time.length < 2);
  const [sameNumberEachRound, toggle] = useState(initialState);
  let inputs = [];
 
  for(let i = 0; i < rounds; i++) {
    const defaultValue: string = !time ? '' : typeof time === 'string' ? time : time[i];
    const name = index === undefined ? `time.${i}` : nestedIndex === undefined ? `movements.${index}.time.${i}` : `movements[${nestedIndex}].movements[${index}].time[${i}]`;
    inputs.push(<TextInput 
      key={`${index}-${i}`}
      name={name}
      label='time'
      defaultValue={defaultValue}
    />)
  }

  const onChange = e => {
    e.preventDefault();
    toggle(!sameNumberEachRound);
  }

  return (
    <>
      { rounds > 1 && 
        <CheckboxWithLabel 
          checked={sameNumberEachRound}
          update={onChange}
          primary={true}
          label="Same length of time each round?"
        /> 
      }
      { sameNumberEachRound ? inputs[0] :  inputs}
    </>
  )
}

export default Time;