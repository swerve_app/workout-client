import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { useField } from 'formik';
import NumberInput from '../form/NumberInput';
import { ITypeMovementRounds } from 'interfaces/iWorkoutForm';
import CheckboxWithLabel from 'components/form/CheckboxWithLabel';


const Reps = (props : ITypeMovementRounds) => {
  const { reps, index, nestedIndex } = props;
  const rounds = props.rounds || props.nestedRounds || 1;
  const initialState = !reps || (reps && reps.length < 2);
  const [sameNumberEachRound, toggle] = useState(initialState);

  const name = index === undefined ? `reps` : nestedIndex === undefined ? `movements.${index}.reps` : `movements[${nestedIndex}].movements[${index}].reps`;
  const [field, meta, helpers] = useField(name);
  const { setValue } = helpers;

  let inputs = [];
  for(let i = 0; i < rounds; i++) {
    const defaultValue = reps ? reps[i] : undefined;

    inputs.push(<Grid item xs={rounds < 2 ? 6 : 2}><NumberInput 
      key={`${index}-${i}`}
      name={`${name}[${i}]`}
      label={'reps'}
      defaultValue={defaultValue}
    /></Grid>)
  }

  const onChange = e => {
    e.preventDefault();
    if (!sameNumberEachRound){
      setValue([ meta.value[0] ]);
    }
    toggle(!sameNumberEachRound);
  }

  return (
    <Grid container item direction='column' xs={12}>
      { rounds > 1 &&
        <Grid item>
          <CheckboxWithLabel 
            checked={sameNumberEachRound}
            update={onChange}
            primary={true}
            label="Same number of reps each round"
          />
        </Grid>
      }
      <Grid item container direction='row'>
      { sameNumberEachRound ? inputs[0] :  inputs}
      </Grid>
    </Grid>
  )
}

export default Reps;