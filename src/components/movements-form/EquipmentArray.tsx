import React from 'react';
import { FieldArray } from 'formik';
import { useField } from 'formik';
import InputLabel from '@material-ui/core/InputLabel';
import Chip from '@material-ui/core/Chip';
import { equipmentEnum } from '../../constants';

const EquipmentArray = ({equipment}: {equipment: string[]}) => {
  const [field, meta, helpers] = useField('equipment');

  const removeFromArray = (name) => {
    const newArray = meta.value.filter(item => item !== name);
    helpers.setValue(newArray);
  };

  const addToArray = name => {
    const newArray = meta.value ? [...meta.value, name] : [name];
    helpers.setValue(newArray);
  };
  
  return (

  <FieldArray
    name='equipment'
    render={() => (
      <>
      <InputLabel>Equipment</InputLabel>
        {equipmentEnum.map((name, index) => {
          const isused = equipment && equipment.includes(name);
          return(
            <Chip 
              key={index}
              style={{ 'margin': '1px 3px'}}
              label={name}
              onClick={ () => isused ? removeFromArray(name) : addToArray(name)}
              variant={isused ? undefined : 'outlined'}
            />
          )
        })} 
      </>
      )
    }
  />

 )
};

export default EquipmentArray;