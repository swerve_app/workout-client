import React, { useState } from 'react';
import { useField } from 'formik';
import Grid from '@material-ui/core/Grid';
import NumberInput from '../form/NumberInput';
import CheckboxWithLabel from 'components/form/CheckboxWithLabel';


const ByGender = (props) => {
  const { sameNumberEachRound, label, name, index = undefined, nestedIndex = undefined, genderArray=[]} = props;
  let inputs = [];
  const rounds = props.rounds || props.nestedRounds || 1;
  for(let i = 0; i < rounds; i++) {
    const defaultValue = genderArray ? genderArray[i] : undefined;
    const inputName = index === undefined ? `${name}.${label}.${i}` : nestedIndex === undefined ? `movements.${index}.${name}.${label}.${i}` : `movements[${nestedIndex}].movements[${index}].${name}.${label}.[${i}]`;

    inputs.push(
      <Grid item xs={6}>
        <NumberInput 
          key={`${index}-${i}-${label}`}
          name={inputName}
          label={i === 0 ? label === 'f' ? `${name}`: `${name} for men` : rounds > 0 ? `${i + 1}` : label === 'f' ? 'women' : 'men'}
          defaultValue={defaultValue}
        />
      </Grid>
    )
  }

  return (
    <Grid container item spacing={1}>
      { sameNumberEachRound ? inputs[0] : inputs}
    </Grid>
  )
}

const ForMen = (props) => {
  return <ByGender {...props} genderArray={props.defaultValue} label='m'/>
}

const ForWomen = (props) => {
  return <ByGender {...props} genderArray={props.defaultValue} label='f'/>
}

const GenderAndRounds = ({defaultValue, ...props}) => {
  const inputName = props.index === undefined ? `${props.name}` : props.nestedIndex === undefined ? `movements.${props.index}.${props.name}` : `movements[${props.nestedIndex}].movements[${props.index}].${props.name}`;

  const [field, meta, helpers] = useField(inputName);
  const { setValue } = helpers;

  const [sameByGender, toggle] = useState(props.sameByGender);

  let value : {f: any[], m: any[]} = defaultValue && defaultValue.f ? defaultValue : 
    !props.index ? {m: [], f: []} : 
      !props.nestedIndex ? `movements[${props.index - 1}].${props.name}` : 
        `movements[${props.nestedIndex}].movements[${props.index - 1}].${props.name}`;
  if (!value.f){
    value = { m: [], f: []}
  }

  const intitialRepState = props.rounds < 1 || !value.f || (value.f && value.f.length < 2);
  const [sameNumberEachRound, toggleSameNumber] = useState(intitialRepState);

  const changeSameByGender = () => {
    if (!sameByGender){
      setValue({m: [], f: meta.value ? meta.value.f : value.f})
    }
    toggle(!sameByGender)
  };

  const changeSameNumber = () => {
    if (!sameNumberEachRound){
      setValue({
        f: [meta.value.f[0]],
        m: meta.value.m ? [meta.value.m[0]] : []
      });
    }
    toggleSameNumber(!sameNumberEachRound);
  };

  return (
    <Grid container item direction='column' spacing={3}>
      <CheckboxWithLabel
        checked={sameByGender}
        update={changeSameByGender}
        label={`Same ${props.name} for everyone?`}
      />

      { props.rounds > 1 &&
        <CheckboxWithLabel
          checked={sameNumberEachRound}
          update={changeSameNumber}
          primary={true}
          label={`Same ${name} each round?`}
        />
      }

      <ForWomen {...props} defaultValue={value.f} sameNumberEachRound={sameNumberEachRound}/>
      { !sameByGender && <ForMen {...props} defaultValue={value.m} sameNumberEachRound={sameNumberEachRound}/>}
      {props.children}
    </Grid>
  )
}

export default GenderAndRounds;