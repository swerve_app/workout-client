import React from 'react';
import { FieldArray } from 'formik';
import { useField } from 'formik';
import Chip from '@material-ui/core/Chip';
import InputLabel from '@material-ui/core/InputLabel';
import { Done } from '@material-ui/icons';
import { scoringEnum } from '../../constants'

const Scored = ({scoring}) => {
  const [field, meta, helpers] = useField('scoring');

  const removeFromArray = (name) => {
    const newArray = meta.value.filter(item => item !== name);
    helpers.setValue(newArray);
  };

  const addToArray = name => {
    const newArray = meta.value ? [...meta.value, name] : [name];
    helpers.setValue(newArray);
  };

  return (
    <FieldArray
      name='scoring'
      render={() => (
        <>
          <InputLabel >Scored by</InputLabel>
          {scoringEnum.map((name, index) => {
            const isused = scoring && scoring.includes(name);
            return(
              <Chip 
                key={index}
                label={name}
                style={{ 'margin': '1px 3px'}}
                clickable
                deleteIcon={isused ? <Done /> : null}
                onClick={ () => isused ? removeFromArray(name) : addToArray(name)}
                variant={isused ? undefined : 'outlined'}
              />
            )
          })} 
        </>
        )
      }
    />
  )
};

export default Scored;