import React from 'react';
import { Field } from 'formik';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import { TextField } from '@material-ui/core';

const filter = createFilterOptions<string>();



const TagsFieldArray = ({tags}) => {
  return (
    <Field
      name='tags'
    >
      {({ form }) => {
        return (<Autocomplete
          defaultValue={tags}
          onChange={(e: any, newValue: string[]) => {
            form.setFieldValue(tags, newValue)
          }}
          filterOptions={(options, params) => {
            const filtered = filter(options, params) as any[];

            if (params.inputValue !== '') {
              filtered.push({
                inputValue: params.inputValue,
                title: `Add "${params.inputValue}"`
              });
            }
            return filtered;
          }}
          getOptionLabel={(option) => {
            // e.g value selected with enter, right from the input
            if (typeof option === 'string') {
              return option;
            }
            if (option.inputValue) {
              return option.inputValue;
            }
          }}
          options={['home gym', 'no equipment', 'track workout']}
          freeSolo
          multiple
          id='tags'
          renderInput={(params) => (
            <TextField {...params} label='tags' margin="normal" variant='outlined'/>
          )}
        />)
        }}
    </Field>
  )
};

export default TagsFieldArray;