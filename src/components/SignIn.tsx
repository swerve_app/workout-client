import React, { useContext } from 'react';
import { SignInSection, SignInText } from '../styles/SignIn';
import { FirebaseContext } from '../contexts/firebase';
import { UserContext } from '../contexts/user';
import { SignInCard } from './SignInCard';

const SignIn = () => {
  const firebase = useContext(FirebaseContext);
  const { user } = useContext(UserContext);

  const providers = firebase && {
    google: firebase.googleProvider.providerId,
    email: firebase.emailProvider.providerId
  };

  return (
    <SignInSection>
      <SignInText>
        <h1>FITNSSD</h1>
        <p>Build your best workout schedule with FITNSSD</p>
      </SignInText>
    {
      user &&  !user.isAnonymous ?
      <SignInText>
       <span>Welcome {user.name}! Get your fitness on</span>
      </SignInText> : 
      <SignInCard 
        firebase={firebase}
        providers={providers}
      />
    }

    </SignInSection>
  )
};

export default SignIn;