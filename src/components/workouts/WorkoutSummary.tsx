import React from 'react';
import Grid from '@material-ui/core/Grid';
import { SummaryWrapper } from '../../styles/Summary';
import { MovementSummary } from '../movements-summary/MovementSummary';
import { IWorkout } from 'interfaces/workout';
import { RoundSummary } from '../movements-summary/RoundSummary';

const Movements = ({movements, id, showNotes, showReps, nested = false}) => {
  if (!movements || movements.length < 1){ return null; }
  return movements.map((movement, index) => {
    return (
      <div key={`${id}-movement-${index}`}>
      <MovementSummary 
        values={movement} 
        key={`${id}-movement-${index}`}
        showNotes={showNotes}
        showReps={showReps}
        nested={nested}
      /> 
      <Movements 
        key={`${id}-movement-${index}-nested`}
        movements={movement.movements} 
        id={`${id}-${index}`} 
        showNotes={showNotes}
        showReps={!movement.sharedRepSchema}
        nested={true}
      />
      </div>
    )
  })
}
const WorkoutSummary = ({wod, showNotes}: {wod: IWorkout, ActionButtons?: any, showNotes?: boolean}) => {
  if (!wod){ return null; }
  const useScharedRepSchema = !!wod.sharedRepSchema && wod.sharedRepSchema.split('-').length > 0;
  return (
    <SummaryWrapper>
      { wod.name && <h3>{wod.name}</h3>}

      <Grid container spacing={0}>
        <RoundSummary 
          show={!wod.sharedRepSchema && wod.type !== 'on the minute'}
          rounds={wod.rounds} 
          string={ 
            wod.type?.includes('rounds') || wod.displayType?.includes('rounds') ? 
            '' : 'rounds'
          }
        />
        <Grid item>
          { wod.type === 'on the minute' ? 
            <h3>every {wod.every} for {wod.time} minutes</h3> :
            <h3>{wod.displayType || wod.type}</h3>
          }
        </Grid>
        { wod.type !== 'on the minute' && 
          <Grid item>
            <h3>{wod.time}</h3>
          </Grid> 
        }
      </Grid>


      {useScharedRepSchema && <p>{ wod.sharedRepSchema }</p>}
      <Movements 
        movements={wod.movements} 
        id={wod._id} 
        showNotes={showNotes}
        showReps={!useScharedRepSchema}
      />
      <p>{wod.notes}</p>
    </SummaryWrapper>
  )
};

export default WorkoutSummary;