import React from 'react';
import { Link } from '@reach/router';
import BookmarkBorderOutlinedIcon from '@material-ui/icons/BookmarkBorderOutlined';
import BookmarkOutlinedIcon from '@material-ui/icons/BookmarkOutlined';
import TimelineIcon from '@material-ui/icons/Timeline';
import { FacebookShareButton } from 'react-share';
import FacebookIcon from '@material-ui/icons/Facebook';
import { workoutBlurb } from '../../functions/summaryHelpers';

export const BookmarkAndEdit = (props) => {
  const { bookmarked, onClick } = props;
  return ( 
    bookmarked ? 
    <BookmarkOutlinedIcon onClick={onClick}/> : 
    <BookmarkBorderOutlinedIcon 
      color='primary'
      onClick={onClick}
    />
  )
};

export const FacebookShare = ({wod}) => {
  if (!wod){ return null; }
  const url = wod?._id ? `https://fitnssd.com/wods/${wod._id}` : 'https://fitnssd.com';
  const quote = wod._id ? workoutBlurb(wod) : 'Get moving!';

  return (
    <FacebookShareButton 
      url={url} 
      quote={quote}
      hashtag='#fitnssd'
    >
      <FacebookIcon />
    </FacebookShareButton>
  )
}

export const WodPageLink = ({id}) => {
  const url = `/wods/${id}`;
  return (
    <Link to={url}>
      <TimelineIcon />
    </Link>
  )
}

const WorkoutButtons = ({wod, bookmarked, onBookmark}) => {
  
  return (
    <div style={{display: 'flex'}}>
      <BookmarkAndEdit 
        bookmarked={ bookmarked } 
        onClick={ onBookmark } 
      />
      
      <FacebookShare wod={wod}/>
      <WodPageLink id={wod._id} />
    </div>
  )
}

export default WorkoutButtons;