import React, { useState } from 'react';
import { Modal } from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      height: '60vh',
      fontSize: '1.5em',
      overflow: 'scroll',
      maxWidth: 400,
      [theme.breakpoints.down('sm')]: {
        width: '80vw'
      },
      [theme.breakpoints.up('lg')]: {
        width: '40%'
      },
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(5, 3),
      borderRadius: '5px'
    },
  }),
);

const SimpleModal = (props) => {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = useState(getModalStyle);


  return (
    <Modal
      open={props.open}
      onClose={props.close}
      aria-labelledby="workout-modal"
      aria-describedby="workout-modal"
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={props.open}>
        <div style={modalStyle} className={classes.paper}>
          {props.children}
        </div>
      </Fade>
    </Modal>
  );
}

export default SimpleModal;