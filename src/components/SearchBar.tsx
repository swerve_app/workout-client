import React from 'react';
import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { ISearchBar } from 'interfaces/search';

const useStyles = makeStyles((theme: Theme) => createStyles({
  paper: {
    width: '50%',
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    },
    [theme.breakpoints.up('md')]: {
      width: '40%'
    }
  }
})
);

export const SearchBar = (props: ISearchBar) => {
  const classes  = useStyles();
  return (
    <Autocomplete
      {...props}
      className={classes.paper}
      multiple
      id={props.label}
      renderInput={(params) => <TextField {...params} label={props.label} margin="normal" variant='outlined'/>}
      getOptionDisabled={() => props.disabled}
    />
  )
}