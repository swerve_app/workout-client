import React from 'react';
import { Link } from '@reach/router';
import { navigate } from '@reach/router';
import { Plans } from './Plans';

export const FitnssdUp = () => {
  return (
    <div>
      <h5>FITNSSD UP</h5>
      <p>You are on the premium plan</p>
    </div>
  )
};

export const FitnssdCore = () => {
  return (
    <div>
      <h5>FITNSSD CORE</h5>
      <p>You are on the core plan</p>
      <Link to='/subscribe'>Subscribe to FITNSSD UP for more top-shape features</Link>
    </div>
  )
};

export const AccountStatus = ({isSub}) => {
  const onSubscribe = () => {
    navigate('/subscribe');
  };

  return (
    <div>
      <h4>Subscription</h4>      
      { isSub ? <FitnssdUp/> : <FitnssdCore/> }
      <Plans isSub={isSub} onSubscribe={onSubscribe}/>
    </div>
  )
}