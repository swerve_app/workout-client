import React from 'react';
import { Grid, Card } from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme: Theme) => 
  createStyles({
    card: {
      textAlign: 'center',
    },
    disabled: {
      textAlign: 'center',
      padding: '0 30px',
      extend: 'card',
    },
    selectable: {
      textAlign: 'center',
      padding: '0 30px',
      extend: 'card',
      cursor: 'pointer',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      backgroundImage: 'url("https://ik.imagekit.io/workout/maryna-yazbeck-Su5W0foVywY-unsplash_dQADtF2KC.jpg")'
    },
    features: {
      textAlign: 'left'
    },
    plans: {
      margin: '3px 0px 3px 0px'
    }
  })
);


const Plan = (props) => {
  const classes = useStyles();
  const className = props.disabled ? 'disabled' : 'selectable';
  const subscribe = e => {
    e.preventDefault();
    if (!props.disabled && props.subscribe){
      // go to subscribe page
      props.subscribe();
    }
  };

  return (
    <Grid item>
      <Card className={classes[className]} onClick={subscribe}>
        <h3>{props.title}</h3>
        <h4>{props.price}</h4>
        <div className={classes.features}>
          {props.features}
        </div>
      </Card>
    </Grid>
  )
};

const CoreFeatures = () => (
  <div>
    <p>Find workouts by</p>
    <ul>
      <li>equipment</li>
    </ul>
    <p>Bookmark 10 workouts</p>
    <p>Record workout score</p>
    <p>Share workout to social media</p>
    <p>Random workout finder</p>
  </div>
);

const UpFeatures = () => (
  <div>
    <p>Find workouts by</p>
    <ul>
      <li>equipment</li>
      <li>movement</li>
      <li>workout type</li>
    </ul>
    <p>Bookmark 50 workouts</p>
    <p>Schedule 20 workouts in the future</p>
    <p>Record workout score</p>
    <p>Share workout to social media</p>
    <p>Random workout finder</p>
    <p>Recommended workouts</p>
  </div>
);

export const Plans = ({isSub, onSubscribe}) => {
  const classes = useStyles();

  return (
    <Grid container spacing={3} className={classes.plans}>
      <Plan 
        title='FITNSSD CORE'
        price='free'
        disabled={!isSub}
        features={<CoreFeatures/>}
      />
      <Plan
        title='FITNSSD UP'
        price='£2/month'
        disabled={isSub}
        subscribe={onSubscribe}
        features={<UpFeatures/>}
      />
    </Grid>
  )
};