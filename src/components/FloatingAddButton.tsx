import React, { useContext } from 'react';
import { Link } from '@reach/router';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { UserContext } from '../contexts/user';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
        position: 'fixed',
        top: 'auto',
        bottom: '40px',
        left: 'auto',
        right: '40px'
      },
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
  }),
);

export default function FloatingAddButton() {
  const classes = useStyles();
  const { user } = useContext(UserContext)
  console.log('floating add button: ', user)

  if (!user || !user.isAdmin){ return null; }
  return (
    <div className={classes.root}>
      <Fab color="primary" aria-label="add">
        <Link to='/add' style={{'color': '#fff', 'lineHeight': '0'}}>
          <AddIcon />
        </Link>
      </Fab>
    </div>
  );
}