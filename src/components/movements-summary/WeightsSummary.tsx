import React from 'react';
import { genderAndRoundsNumbers } from '../../functions/summaryHelpers';

export const WeightsSummary = ({weight, weight_units, units}) => {
  if (!weight){ return null; }
  return (
    <p>
      {genderAndRoundsNumbers(weight) } {' '}
      { weight_units || units || 'lbs' }
    </p>
  )
}