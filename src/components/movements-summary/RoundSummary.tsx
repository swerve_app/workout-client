import React from 'react';
import Grid from '@material-ui/core/Grid';


export const RoundSummary = ({rounds, string, show, tag = 'h3'}) => {
  if (!rounds || !show){ return null; }
  return ( 
    <Grid item>
      {
        tag === 'h3' ? 
        <h3>
          {rounds} {string}
        </h3>
        :
        <p>
          {rounds} {string}
        </p>
      }
    </Grid>
  )
}