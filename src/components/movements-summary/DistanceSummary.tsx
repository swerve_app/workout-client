import React from 'react';
import { genderAndRoundsNumbers } from '../../functions/summaryHelpers';

export const DistanceSummary = ({distance, distance_units, units}) => {
  if (!distance){ return null; }
  return (
    <p>
      {genderAndRoundsNumbers(distance) } {' '}
      { distance_units || units || 'm' }
    </p>
  )
}