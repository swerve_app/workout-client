import React from 'react';
import Grid from '@material-ui/core/Grid';
import { RoundSummary } from './RoundSummary';
import { DistanceSummary } from './DistanceSummary';
import { 
  showType, 
  genderAndRoundsNumbers, 
  repsNumbers 
} from '../../functions/summaryHelpers'; 
import { WeightsSummary } from './WeightsSummary';


export const MovementSummary = ({values, showNotes, showReps, nested}) => {
  const style = {
    'marginLeft': nested ? '15px' : '0px'
  };
  const useScharedRepSchema = !!values.sharedRepSchema && values.sharedRepSchema.split('-').length > 0;

  return (
    <Grid container style={style} spacing={0}>
      <RoundSummary 
        show={!values.sharedRepSchema && values.type !== 'on the minute'}
        rounds={values.rounds}
        string={values.type === 'sets' ? 'sets' : 'rounds'}
        tag='p'
      />
      {useScharedRepSchema && <p>{ values.sharedRepSchema }</p>}

      <Grid item>
        { values.cals && <p>{genderAndRoundsNumbers(values.cals)} </p>} {' '}
      </Grid>
      <Grid item>
        { showType(values.type) && <p>{values.type }</p>}
      </Grid>
  
      <Grid item>
        <p>
          { showReps && values.reps && repsNumbers(values.reps)} {' '}
          { values.movement && values.movement} 
        </p>
      </Grid>
      <Grid item>

        <DistanceSummary 
          distance={values.distance} 
          distance_units={values.distance_units}
          units={values.units}
        />
      </Grid>
      <Grid item>
        { values.time && <p>{values.time}</p>}
      </Grid>
      <Grid item>
        <WeightsSummary
          weight={values.weight}
          weight_units={values.weight_units}
          units={values.units}
        />
      </Grid>
    </Grid>
  )
};