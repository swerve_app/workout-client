import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { navigate } from '@reach/router';
import { UserContext } from '../contexts/user';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    maxWidth: 300
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  }
});


export const SignInCard = ({ firebase, providers }) => {
  const classes = useStyles();
  const { updateUser } = useContext(UserContext)
  
  const uiConfig: any = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'popup',
    signInOptions: providers && [
      {
        provider: providers.google,
        customParameters: {
          // Forces account selection even when one account
          // is available.
          prompt: 'select_account'
        }
      },
      providers.email
    ],
    autoUpgradeAnonymousUsers: true,

    // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
    // signInSuccessUrl: '/success',
    // tosUrl: '/tocs',
    // privacyPolicyUrl: '/tocs'
    callbacks: {
      signInFailure: function(err){
        console.log('error signing in', err)
      },
      signInSuccessWithAuthResult: function (authResult){
        const { user } = authResult;

        updateUser({
          isSignedIn: true,
          isAnon: false,
          uid: user.uid,
          name: user.name
        });

        if (authResult.additionalUserInfo.isNewUser){
          navigate('/profile');
        }
        else {
          navigate('/userWorkouts')
        }
        // Do something with the returned AuthResult.
        // Return type determines whether we continue the redirect automatically
        // or whether we leave that to developer to handle.
        return false;
      },
    }
  };

  return (
    <Card className={classes.root}>
      <StyledFirebaseAuth 
        uiConfig={uiConfig} 
        firebaseAuth={firebase.auth}
      />
    </Card>
  )
}