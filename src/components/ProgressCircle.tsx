import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const ProgressCircle = (props) => {
  if (!props.loading){ return null; }
  return (
    <CircularProgress /> 
  )
}

export default ProgressCircle;